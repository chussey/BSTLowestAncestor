/*************************************************************************
 *  Binary Search Tree class.
 *  Adapted from Sedgewick and Wayne.
 *
 *  @version 3.0 1/11/15 16:49:42
 *
 *  @author TODO
 *
 *************************************************************************/

import java.util.NoSuchElementException;

public class BST<Key extends Comparable<Key>, Value> {
    private Node root;             // root of BST
    Node prevNode = null;

    /**
     * Private node class.
     */
    private class Node {
        private Key key;           // sorted by key
        private Value val;         // associated data
        private Node left, right;  // left and right subtrees
        private int N;             // number of nodes in subtree

        public Node(Key key, Value val, int N) {
            this.key = key;
            this.val = val;
            this.N = N;
            
        }
    }

    // is the symbol table empty?
    public boolean isEmpty() { return size() == 0; }
    
    public Node returnRoot()
    {
    	Node newRoot = root;
    	return newRoot;
    }
    // return number of key-value pairs in BST
    public int size() { return size(root); }

    // return number of key-value pairs in BST rooted at x
    private int size(Node x) {
        if (x == null) return 0;
        else return x.N;
    }

    /**
     *  Search BST for given key.
     *  Does there exist a key-value pair with given key?
     *
     *  @param key the search key
     *  @return true if key is found and false otherwise
     */
    public boolean contains(Key key) {
        return get(key) != null;
    }

    /**
     *  Search BST for given key.
     *  What is the value associated with given key?
     *
     *  @param key the search key
     *  @return value associated with the given key if found, or null if no such key exists.
     */
    public Value get(Key key) { return get(root, key); }

    private Value get(Node x, Key key) {
        if (x == null) return null;
        int cmp = key.compareTo(x.key);
        if      (cmp < 0) return get(x.left, key);
        else if (cmp > 0) return get(x.right, key);
        else              return x.val;
    }

    /**
     *  Insert key-value pair into BST.
     *  If key already exists, update with new value.
     *
     *  @param key the key to insert
     *  @param val the value associated with key
     */
    public void put(Key key, Value val) {
        if (val == null) { delete(key); return; }
        root = put(root, key, val);
    }

    private Node put(Node x, Key key, Value val) {
        if (x == null) return new Node(key, val, 1);
        int cmp = key.compareTo(x.key);
        if      (cmp < 0) x.left  = put(x.left,  key, val);
        else if (cmp > 0) x.right = put(x.right, key, val);
        else              x.val   = val;
        x.N = 1 + size(x.left) + size(x.right);
        return x;
    }

    /**
     * Tree height.
     *
     * Asymptotic worst-case running time using Theta notation: worst case running time for
     * height function on a BST is theta(N) if the tree is built degenerately and is random.
     * 
     * 
     * 
     * @return the number of links from the root to the deepest leaf.
     *
     * Example 1: for an empty tree this should return -1.
     * Example 2: for a tree with only one node it should return 0.
     * Example 3: for the following tree it should return 2.
     *   B
     *  / \
     * A   C
     *      \
     *       D
     */
    public int height() {
        return height(root);
    }
    private int height(Node heightNode) {
        if (heightNode == null)
        {
        	return -1;
        }
        else
        {
        return 1 + Math.max(height(heightNode.right), height(heightNode.left));
        }
    }
    
    
    /**
     * Median
     * @param newNode
     * @param medianKey
     * @return
     */
    private Node median(Node newNode, int medianKey) {
        
        if (medianKey==-1)
    	{
    		Node x = root;
    		return x;
    	}
        int branchLeft = size(newNode.left);
        if(branchLeft > medianKey) 
        {
        	return median(newNode.left, medianKey);
        }
        else if(branchLeft < medianKey)
        {
        	 return median(newNode.right,medianKey-branchLeft-1);
        }
        return newNode;  
    }
    public Key median() {
        if (isEmpty())
        {
        	return null;
        }
        int medianKey = size() / 2;
        if (medianKey%2==0&&size()>3)
        {
        	medianKey--;
        }
        else if (size()==1)
        {
      	  medianKey = -1;
      	  Node keyNode = median(root, medianKey);
      	  return keyNode.key;
        }

        Node keyNode = median(root, medianKey);
        
        return keyNode.key;
    }
    

    /**
     * Print all keys of the tree in a sequence, in-order.
     * That is, for each node, the keys in the left subtree should appear before the key in the node.
     * Also, for each node, the keys in the right subtree should appear before the key in the node.
     * For each subtree, its keys should appear within a parenthesis.
     *
     * Example 1: Empty tree -- output: "()"
     * Example 2: Tree containing only "A" -- output: "(()A())"
     * Example 3: Tree:
     *   B
     *  / \
     * A   C
     *      \
     *       D
     *
     * output: "((()A())B(()C(()D())))"
     *
     * output of example in the assignment: (((()A(()C()))E((()H(()M()))R()))S(()X()))
     *
     * @return a String with all keys in the tree, in order, parenthesized.
     */
    public String printKeysInOrder() {
    	String orderedString ="";
    	
      if (isEmpty()==true)
      {
    	  orderedString=("()");
    	  return orderedString;
      }
     orderedString+="(";
     orderedString+=doOrdering(root,orderedString);
     orderedString+=")";
     return orderedString;
     
      
    }
    private String doOrdering(Node x, String orderedString)
    {
    	if (x == null)
    	{
    		return "";
    	}
    	else
    	{
    		return "(" +doOrdering(x.left,orderedString)+")"+ x.key +"("+doOrdering(x.right,orderedString)+")";
    	}
    }
    
    /**
     * Pretty Printing the tree. Each node is on one line -- see assignment for details.
     *
     * @return a multi-line string with the pretty ascii picture of the tree.
     */
    public String prettyPrintKeys() {
      if(isEmpty()==true)
      {
    	  return "-null\n";
      }
      String prettyString="";
      String newPrintKeys =prettyPrinter(root,prettyString);
      return newPrintKeys;
    }
    private String prettyPrinter(Node keyNode, String prettyString)
    {
    	if(keyNode==null)
    	{
    		return prettyString + "-null\n";
    	}
    	return prettyString + "-" + keyNode.val + "\n" + prettyPrinter(keyNode.left, prettyString + " |") 
    	+ prettyPrinter(keyNode.right, prettyString + "  ");
    }
    
    

    /**
     * Deletes a key from a tree (if the key is in the tree).
     * Note that this method works symmetrically from the Hibbard deletion:
     * If the node to be deleted has two child nodes, then it needs to be
     * replaced with its predecessor (not its successor) node.
     *
     * @param key the key to delete
     */
    public void delete(Key key)
    {
    	root=delete(root,key);
    }
    private Node deleteMaxKey(Node deleteNode) {
        if (deleteNode.right == null) 
        {
        	return deleteNode.left;
        }
        deleteNode.right = deleteMaxKey(deleteNode.right);
        deleteNode.N = size(deleteNode.left)+size(deleteNode.right)+1;
        return deleteNode;
    }
    private Node delete(Node deleteNode, Key key)
    {
        if(deleteNode == null)
        {
                return null;
        }
        int cmpkey = key.compareTo(deleteNode.key);
        if (cmpkey < 0)                                                     
        {
                deleteNode.left = delete(deleteNode.left, key);
        }
        else if(cmpkey > 0)
        {
        	deleteNode.right = delete(deleteNode.right, key);
        }
        else
        {
                if (deleteNode.right == null)                    
                {
                        return deleteNode.left;
                }
                if (deleteNode.left == null)                     
                {
                        return deleteNode.right;
                }
                Node toBeDeleted = deleteNode;                                            
                deleteNode = maxKeyNode(toBeDeleted.left);                   
                deleteNode.left = deleteMaxKey(toBeDeleted.left);            
                deleteNode.right = toBeDeleted.right;
        }
        deleteNode.N=size(deleteNode.left)+size(deleteNode.right)+1;                         
        return deleteNode;
    }
    private Node maxKeyNode (Node maxNode)
    {
    	if (maxNode.right==null)
    	{
    		return maxNode;
    	}
    	else
    	{
    		return maxKeyNode(maxNode.right);
    	}
    }
    
    public Key lowestCommonAncestor(int x, int y){
    	if (root == null)
        	return null;
      /**
    	if (x==y){
    		Key key2=root.key;
    			return key2;
    	}
    	**/
    	Node root=returnRoot();
    	Node p=select(root,x);
    	Node q=select(root,y);
    	
    	Node solution = lowestCommonAncestor(root,x,y);
    	Key sol1= solution.key;
    	return sol1;
    }
    Node lowestCommonAncestor(Node root, int n1, int n2) {
    	// Base case
    	if (root == null)
    	return null;
    	

    	// If either n1 or n2 matches with root's key, report
    	// the presence by returning root (Note that if a key is
    	// ancestor of other, then the ancestor key becomes LCA
    	Integer x = (Integer) root.key;
    	if (x == n1 || x == n2)
    	return root;

    	// Look for keys in left and right subtrees
    	Node left_lca = lowestCommonAncestor(root.left, n1, n2);
    	Node right_lca = lowestCommonAncestor(root.right, n1, n2);

    	// If both of the above calls return Non-NULL, then one key
    	// is present in once subtree and other is present in other,
    	// So this node is the LCA

    	if (left_lca != null && right_lca != null)
    	return root;

    	// Otherwise check if left subtree or right subtree is LCA
    	return (left_lca != null) ? left_lca : right_lca;
    	}
    public Key select(int n) {
    	if (n < 0 || n >= size()) return null;
    	Node x = select(root, n);
    	return x.key;
    	}
    
    private Node select(Node x, int n) {
    	if (x == null) return null;
    	int t = size(x.left);
    	if (t > n) return select(x.left, n);
    	else if (t < n) return select(x.right, n-t-1);
    	else return x;
    	}
    
}