import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;





//-------------------------------------------------------------------------
/**
 *  Test class for Doubly Linked List
 *
 *  @version 3.1 09/11/15 11:32:15
 *
 *  @author  Conor Hussey
 */

@RunWith(JUnit4.class)
public class BSTTest
{
  
  //TODO write more tests here.
	
  
  /** <p>Test {@link BST#prettyPrintKeys()}.</p> */
      
  
     /** <p>Test {@link BST#delete(Comparable)}.</p> */
     @Test
     public void testDelete() {
         BST<Integer, Integer> bst = new BST<Integer, Integer>();
         bst.put(7, 7);   
         bst.put(8, 8);   
         bst.put(3, 3);   
         bst.put(1, 1);   
         bst.put(2, 2);   
         bst.delete(1);
         assertEquals("Checking order of constructed tree",
                 "(((()2())3())7(()8()))", bst.printKeysInOrder());
         
         bst.delete(7);
         bst.delete(8);
         bst.delete(3);
         bst.delete(2);
         
         bst.delete(1);
         assertEquals("Deleting from empty tree", "()", bst.printKeysInOrder());
         
         bst.put(7, 7);   //        _7_
         bst.put(8, 8);   //      /     \
         bst.put(3, 3);   //    _3_      8
         bst.put(1, 1);   //  /     \
         bst.put(2, 2);   // 1       6
         bst.put(6, 6);   //  \     /
         bst.put(4, 4);   //   2   4
         bst.put(5, 5);   //        \
                          //         5
         
         assertEquals("Checking order of constructed tree",
                 "(((()1(()2()))3((()4(()5()))6()))7(()8()))", bst.printKeysInOrder());
         
         bst.delete(9);
         assertEquals("Deleting non-existent key",
                 "(((()1(()2()))3((()4(()5()))6()))7(()8()))", bst.printKeysInOrder());
 
         bst.delete(8);
         assertEquals("Deleting leaf", "(((()1(()2()))3((()4(()5()))6()))7())", bst.printKeysInOrder());
 
         bst.delete(6);
         assertEquals("Deleting node with single child",
                 "(((()1(()2()))3(()4(()5())))7())", bst.printKeysInOrder());
 
         bst.delete(3);
         assertEquals("Deleting node with two children",
                 "(((()1())2(()4(()5())))7())", bst.printKeysInOrder());
         
         
     }
     
     @Test
     public void testHeight(){
    	 
	     BST<Integer, Integer> bst = new BST<Integer, Integer>();
	     int expectedResult = -1;
	     assertEquals("Checking height empty tree", expectedResult, bst.height());
	    	 
	     expectedResult=0 ;
	     bst.put(7, 7);
	     assertEquals("Checking height of one node", expectedResult, bst.height());
	     
	     bst.put(8, 8);
	     bst.put(9, 9);
	     expectedResult=2;
	     assertEquals("Checking height of two nodes", expectedResult, bst.height());
	     }
	     
     @Test
     public void isEmptyTest()
     {
    	 boolean expectedResult = true;
    	 BST<Integer, Integer> bst = new BST<Integer, Integer>();
    	 assertEquals("Checking empty tree", expectedResult, bst.isEmpty());
    	 
    	 expectedResult = false;
    	 bst.put(7, 7);
    	 assertEquals("Checking non-empty tree", expectedResult, bst.isEmpty());
     }
     
     @Test
     public void sizeTest()
     {
    	 int expectedResult =0;
    	 BST<Integer, Integer> bst = new BST<Integer, Integer>();
    	 assertEquals("Checking empty tree", expectedResult, bst.size());
    	 
    	 expectedResult=1;
    	 bst.put(7, 7);
    	 assertEquals("Checking one pair", expectedResult, bst.size());
    	 
    	 bst.put(8, 8);
    	 expectedResult=2;
    	 assertEquals("Checking two pairs", expectedResult, bst.size());
     }
     
     @Test
     public void keyTest()
     {
    	 // bst.contains TEST
    	 boolean expectedBoolean=false;
    	 BST<Integer, Integer> bst = new BST<Integer, Integer>();
    	 assertEquals("Checking nothing", expectedBoolean, bst.contains(7));
    	 
    	 String expectedNull = null;
    	 assertEquals("Checking nothing", expectedNull, bst.get(7));
    	 
    	 expectedBoolean = true;
    	 bst.put(7, 8);
    	 assertEquals("Checking two pairs", expectedBoolean, bst.contains(7));
    	 
    	 Integer expectedValue = 8;
    	 assertEquals("Checking two pairs", expectedValue, bst.get(7));
    	 
    	 bst.put(7, 7);   //        _7_
         bst.put(8, 8);   //      /     \
         bst.put(3, 3);   //    _3_      8
         bst.put(1, 1);   //  /     \
         bst.put(2, 2);   // 1       6
         bst.put(6, 6);   //  \     /
         bst.put(4, 4);   //   2   4
         bst.put(5, 5);   //        \
                          //         5
         
         expectedValue=5;
         assertEquals("Checking two pairs", expectedValue, bst.get(5));
     }
     @Test
     public void putTest(){
    	 BST<Integer, Integer> bst = new BST<Integer, Integer>();
    	 bst.put(7,  null);
    	 boolean expectedBoolean=false;
    	 assertEquals("Checking null value", expectedBoolean, bst.contains(7));
    	 
    	 bst.put(7, 7);   //        _7_
         bst.put(8, 8);   //      /     \
         bst.put(3, 3);   //    _3_      8
         bst.put(1, 1);   //  /     \
         bst.put(2, 2);   // 1       6
         bst.put(6, 6);   //  \     /
         bst.put(4, 4);   //   2   4
         bst.put(5, 5);   //        \
         String x = bst.printKeysInOrder();
         //System.out.println(x);
         assertEquals("Checking order of constructed tree",
                 "(((()1(()2()))3((()4(()5()))6()))7(()8()))", bst.printKeysInOrder());
         bst.put(2, 20);
         String y = bst.printKeysInOrder();
         //System.out.println(y);
         assertEquals("Checking order of constructed tree",
                 "(((()1(()2()))3((()4(()5()))6()))7(()8()))", bst.printKeysInOrder());
         
    	 
     }
     @Test
     public void medianTestOneNode(){
    	 BST<Integer, Integer> bst = new BST<Integer, Integer>();
    	 bst.put(7, 8);
    	 Integer expectedKey1 = 7;
    	 assertEquals("Checking null value", expectedKey1, bst.median());  
     }
     @Test
     public void medianTest()
     {
    	 BST<Integer, Integer> bst = new BST<Integer, Integer>();
    	 String expectedString=null;
    	 assertEquals("Checking null value", expectedString, bst.median());
    	 /**
    	 bst.put(1, 1);
    	 Integer expectedKey1 = 1;
    	 assertEquals("Checking null value", expectedKey1, bst.median());  
    	 bst.delete(1);
    	**/
    	 bst.put(7, 7);
         bst.put(8, 8);  
         bst.put(3, 3);  
         bst.put(1, 1);   
         bst.put(2, 2);   
         bst.put(6, 6);
         bst.put(4, 4);
         bst.put(5, 5);
         
         
         Integer expectedKey = 4;
         
         
         assertEquals("Checking null value", expectedKey, bst.median());
    	 
     }
     @Test
     public void lowestTest(){
    	 BST<Integer, Integer> bst = new BST<Integer, Integer>();
    	 String expectedString=null;
    	 Integer expectedKey = null;
    	 int a=0;
         int b=0;
         
         //testing null tree
         assertEquals("",null, bst.lowestCommonAncestor(a,b));
         
         
         
         
         
    	 bst.put(7, 7);   //        _7_
         bst.put(8, 8);   //      /     \
         expectedKey=7;
         a=7;

         b=7;
         
         //testing same root is still lowest ancestor
         assertEquals("",expectedKey, bst.lowestCommonAncestor(a,b));
         
         a=7;
         b=8;
         expectedKey=7;
         
       //testing tree with 2 nodes where 7 will be the lowest common
         assertEquals("",expectedKey, bst.lowestCommonAncestor(a,b));
         
         bst.put(3, 3);   //    _3_      8
         a=3;
         b=8;
         expectedKey=7;
         
       //testing tree with 3 nodes, a and b on the same level
         assertEquals("",expectedKey, bst.lowestCommonAncestor(a,b));
         
         bst.put(1, 1);   //  /     \
         bst.put(2, 2);   // 1       6
         bst.put(6, 6);   //  \     / \
         bst.put(4, 4);   //   2   4   
         bst.put(5, 5);   //        \
         				//			5
         expectedKey = 3;
          a=2;
          b=4;
          
          //testing big tree with nodes of varying height
         assertEquals("",expectedKey, bst.lowestCommonAncestor(a,b));
     }
}