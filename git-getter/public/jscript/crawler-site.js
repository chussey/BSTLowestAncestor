var freqUpd = 50; 

var bigNode = 500;

var indexNodes = 1;

var gitProfile = {
    loginName: sessionStorage.getItem('username'),
    data: null,
    cont: [], //contributors
	gitRepos: null
}
function printData() {
    var html =  '<div class="media">';
    
    html +=         '<div class="media-body">';
    html +=             '<h1 class="mt-0">' + sessionStorage.getItem('username') + '</h1>';
    html +=         '</div>';
    html +=     '</div>';

    html += '<table class="table">';
    $.each(gitProfile.data, function(key, value){
        html += '<tr>';
        html += '<td>' + key + '</td>';
        html += '<td>' + value + '</td>';
        html += '</tr>';
    });
    html += '</table>';

    $("#results").html(html);
}

window.onload = function() {
    funcData();
}


function printRepos() {
    var html =  '<h1>Chussey Repo</h1>';
    html +=     '<table class="table">';
    for (var i in gitProfile.gitRepos) {
        html += '<tr>';
        html += '<td><a href="' + gitProfile.gitRepos[i].html_url + '">' + gitProfile.gitRepos[i].name + '</a>';
        if (gitProfile.gitRepos[i].owner.login!== gitProfile.loginName)
            html += ' <span class="badge badge-pill badge-secondary">' + gitProfile.gitRepos[i].owner.login + '</span>';
        if (gitProfile.gitRepos[i].private)
            html += ' <span class="badge badge-pill badge-info">Private</span>';
        html += '</td>';
        html += '</tr>';
    }
    html += '</table>';
    var windowWidth = $("#results").width();
    html += '<h3>Repo Pie</h3>'
    html += '<svg id="pie-chart" width="' + windowWidth + '" height="' + 3*(windowWidth/2) + '"></svg>';
    $("#results").html(html);
    chartDraw([{repo: null, commits: 1}]);
    var rComs = [];
    for (var i in gitProfile.gitRepos) {
        rComs.push({"repo": gitProfile.gitRepos[i].name, "commits": 0});
        indexComs(rComs, gitProfile.gitRepos[i].url, rComs[i], i == gitProfile.gitRepos.length-1);
    }
}

function indexComs(rComs, url, newOb, lastIteration) {
    $.ajax({
        url: url + '/commits',
        method: "GET",
        data: {
            "access_token": sessionStorage.getItem('token')
        },
        success: function(data, textStatus, jqXHR) {
            newOb.commits = data.length;

            if (lastIteration)
                chartDraw(rComs);
        },
    });
}

function funcData() {
    $.ajax({
        url: "https://api.github.com/users/" + gitProfile.loginName,
        method: "GET",
        data: {
            "access_token": sessionStorage.getItem('token')
        },
        success: function(data, textStatus, jqXHR) {
            $('#user-github-link').attr("href", data.html_url);

            gitProfile.data = data;
            funcRepos();
        }
    });
}


function JSONadd(json, source, variableX) {
    var target = nodeInd(json, variableX);
    if (target == -1) {
        var node = {
            name: variableX.loginName,
            group: 1
        }
        json.nodes.push(node);
        target = json.nodes.length-1;
    }

    var link = {
        source: source,
        target: target,
        value: 1
    }
    json.links.push(link);

    for (var i = 0; i < variableX.cont.length; i++)
        JSONadd(json, target, variableX.cont[i]);
}

function nodeInd(json, variableX) {
    for (var i = 0; i < json.nodes.length; i++)
        if (json.nodes[i].name == variableX.loginName)
            return i;
    return -1;
}

function getRepos(cont, count) {
    var variableX = cont[count];

    if (variableX == null) return;

    $.ajax({
        url: "https://api.github.com/users/" + variableX.loginName + "/repos",
        method: "GET",
        data: {
            "access_token": sessionStorage.getItem('token')
        },
        success: function(data, textStatus, jqXHR) {
            variableX.gitRepos = data;

            if (count+1 < cont.length)
                getRepos(cont, count+1);
            else { 
                for (var i = 0; i < cont.length; i++)
                    repoConAdd(cont[i], 0);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
             console.log(errorThrown);
        }
    });

}

function repoConAdd(variableX, count) {
    var repo = variableX.gitRepos[count];

    if (repo == null) return;  
    $.ajax({
        url: repo.contributors_url,
        method: "GET",
        data: {
            "access_token": sessionStorage.getItem('token')
        },
        success: function(data, textStatus, jqXHR) {
            if (data != null) {
                var cont = data;

                for (var i = 0; (i < cont.length) && (indexNodes < bigNode); i++) {
					
                    if (variableX.loginName != cont[i].login && newCont(cont[i].login, variableX.cont)) {
						
                        var newUser = {
                            loginName: cont[i].login,
                            gitRepos: null,
                            cont: []
                        }

                        variableX.cont.push(newUser);
                        indexNodes++;
                        if ($('#graph').length != 0) {
                            if (indexNodes % freqUpd == 0)
                                showMyContributors();
                        }
						
                    }
					
                }
				
            }

            if (indexNodes < bigNode) {
				
                if (count+1 < variableX.gitRepos.length)
					
                    repoConAdd(variableX, count+1); 
                else 
                    getRepos(variableX.cont, 0)
            }
        },
        
    });

}

function newCont(loginName, cont) {
    for (var i = 0; i < cont.length; i++) {
        if (loginName === cont[i].loginName)
            return false;
    }
    return true;
}
function funcRepos() {

    $.ajax({
        url: "https://api.github.com/user/repos",
        method: "GET",
        data: {
            "access_token": sessionStorage.getItem('token')
        },
        success: function(data, textStatus, jqXHR) {
            gitProfile.gitRepos = data;
        }
    });
}