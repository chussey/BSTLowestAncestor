// similar version of http://bl.ocks.org/dbuezas/9306799

var scale = 0.5;

var svg, width, height;
var width;
var height;
var chartHeight;
var chartRadius;
var arc;
var outerArc;
var chartWidth;
var pie;
function chartDraw(input) {
    svg = d3.select("svg");
    width  = parseInt(svg.style("width"), 10);
    height = parseInt(svg.style("height"), 10);

    chartWidth  = width * scale;
    chartHeight = height * scale;
    chartRadius    = chartWidth/2;

    svg = svg.append("g");
    svg.append("g")
      .attr("class", "slices");
    svg.append("g")
      .attr("class", "labels");
    svg.append("g")
      .attr("class", "lines");

    pie = d3.layout.pie()
      .sort(null)
      .value(function(d) {
        return d.commits;
    });

    arc = d3.svg.arc()
      .outerRadius(chartRadius * 0.7) 
      .innerRadius(0);           

    outerArc = d3.svg.arc()
      .innerRadius(chartRadius) 
      .outerRadius(chartRadius);

    svg.attr("transform", "translate(" + width/2 + "," + height/2 + ")");

    change(input);
}

function change(input) {
  
  var slice = svg.select(".slices").selectAll("path.slice")
  .data(pie(input));

  slice.enter()
    .insert("path")
    .style("fill", function getRandomColour() {
      var letters = '0123456789ABCDEF';
      var colour = '#';
    })
    .attr("class", "slice");

  slice
    .transition().duration(1000)
    .attrTween("d", function(d) {
    this._current = this._current || d;
    var interpolate = d3.interpolate(this._current, d);
    this._current = interpolate(0);
    return function(t) {
      return arc(interpolate(t));
    };
  })

  slice.exit()
    .remove();

  /* ------- TEXT LABELS -------*/
  var text = svg.select(".labels").selectAll("text")
  .data(pie(input));

  text.enter()
    .append("text")
    .attr("dy", ".50em")
    .text(function(d) {
    return d.data.repo;
  });

  function midAngle(d){
    return d.startAngle + (d.endAngle - d.startAngle)/2;
  }

  text.transition().duration(1000)
    .attrTween("transform", function(d) {
    this._current = this._current || d;
    var interpolate = d3.interpolate(this._current, d);
    this._current = interpolate(0);
    return function(t) {
      var d2 = interpolate(t);
      var pos = outerArc.centroid(d2);
      pos[0] = chartRadius * (midAngle(d2) < Math.PI ? 1 : -1);
      return "translate("+ pos +")";
    };
  })
    .styleTween("text-anchor", function(d){
    this._current = this._current || d;
    var interpolate = d3.interpolate(this._current, d);
    this._current = interpolate(0);
    return function(t) {
      var d2 = interpolate(t);
      return midAngle(d2) < Math.PI ? "start":"end";
    };
  });

  text.exit()
    .remove();

  /* ------- SLICE TO TEXT POLYLINES -------*/
  if (input[0].repo != null) {
      var polyline = svg.select(".lines").selectAll("polyline")
      .data(pie(input));

      polyline.enter()
        .append("polyline");

      polyline.transition().duration(1000)
        .attrTween("points", function(d){
        this._current = this._current || d;
        var interpolate = d3.interpolate(this._current, d);
        this._current = interpolate(0);
        return function(t) {
          var d2 = interpolate(t);
          var pos = outerArc.centroid(d2);
          pos[0] = chartRadius * (midAngle(d2) < Math.PI ? 1 : -1);
          return [arc.centroid(d2), outerArc.centroid(d2), pos];
        };
      });

      polyline.exit()
        .remove();
    }
};