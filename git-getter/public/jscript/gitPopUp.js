function gitPopUp() {
    var provider = new firebase.auth.GithubAuthProvider();
    provider.addScope('repo');
    firebase.auth().signInWithPopup(provider).then(function(result) {
        var username = result.additionalUserInfo.username;
        var token = result.credential.accessToken;
        sessionStorage.setItem('username', username);
        sessionStorage.setItem('token', token);
        location.href = "getPage.html";
    });
}

