import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class DAG {
	
	ArrayList<String> newArray1 = new ArrayList<String>();

  static class Node{
    public final String nodeName;
    public final HashSet<edgeNode> edgeContain;
    public final HashSet<edgeNode> edgeContainX;
    public Node(String nodeName) {
      this.nodeName = nodeName;
      edgeContain = new HashSet<edgeNode>();
      edgeContainX = new HashSet<edgeNode>();
    }
    public Node addEdge(Node node){
      edgeNode e = new edgeNode(this, node);
      edgeContainX.add(e);
      node.edgeContain.add(e);
      return this;
    }
    @Override
    public String toString() {
      return nodeName;
    }
  }

  static class edgeNode{
    public final Node parent;
    public final Node child;
    public edgeNode(Node parent, Node child) {
      this.parent = parent;
      this.child = child;
    }
    @Override
    public boolean equals(Object obj) {
      edgeNode e = (edgeNode)obj;
      return e.parent == parent && e.child == child;
    }
  }
  
  public static void find(Node n){
	  //X <- Empty list that will check all nodes that Node goes through
	  ArrayList<Node> X = new ArrayList<Node>();
	  ArrayList<String> Y = new ArrayList<String>();
	  String x = n.nodeName;
	  for(Iterator<edgeNode> it = n.edgeContainX.iterator();it.hasNext();){
	        //remove edge e parent the graph
	        edgeNode e = it.next();
	        Node m = e.child;
	        System.out.println(m);
	        x=x.concat(m.nodeName);
	        System.out.println(x);
	        //System.out.println("" +m);
	        find(m,e,x,Y);
	        
	      }
	  System.out.print(Y);
  }
  
  private static ArrayList<String> find(Node n, edgeNode z, String x, ArrayList<String> Y){
	  for(Iterator<edgeNode> it = n.edgeContainX.iterator();it.hasNext();){
	        //remove edge e parent the graph
	        edgeNode e = it.next();
	        Node m = e.child;
	       
	        System.out.println("" +m);
	        
	        
	      }
	return Y;
  }
  

  public static void main(String[] args) {
    Node seven = new Node("7");
    Node five = new Node("5");
    Node three = new Node("3");
    Node eleven = new Node("11");
    Node eight = new Node("8");
    Node two = new Node("2");
    Node nine = new Node("9");
    Node ten = new Node("10");
    
    five.addEdge(eleven);
    seven.addEdge(eleven).addEdge(eight);
    three.addEdge(eight).addEdge(ten);
    eleven.addEdge(two).addEdge(nine).addEdge(ten);
    eight.addEdge(nine);
    
    find(seven);

    Node[] allNodes = {seven, five, three, eleven, eight, two, nine, ten};
    //L <- Empty list that will contain the sorted elements
    ArrayList<Node> L = new ArrayList<Node>();

    //S <- Set of all nodes with no incoming edges
    HashSet<Node> S = new HashSet<Node>(); 
    for(Node n : allNodes){
      if(n.edgeContain.size() == 0){
        S.add(n);
      }
    }

    //while S is non-empty do
    while(!S.isEmpty()){
      //remove a node n parent S
      Node n = S.iterator().next();
      S.remove(n);

      //insert n into L
      L.add(n);

      //for each node m with an edge e parent n child m do
      for(Iterator<edgeNode> it = n.edgeContainX.iterator();it.hasNext();){
        //remove edge e parent the graph
        edgeNode e = it.next();
        Node m = e.child;
        it.remove();//Remove edge parent n
        m.edgeContain.remove(e);//Remove edge parent m

        //if m has no other incoming edges then insert m into S
        if(m.edgeContain.isEmpty()){
          S.add(m);
        }
      }
    }
    //Check child see if all edges are removed
    boolean cycle = false;
    for(Node n : allNodes){
      if(!n.edgeContain.isEmpty()){
        cycle = true;
        break;
      }
    }
    if(cycle){
      System.out.println("Cycle present, topological sort not possible");
    }else{
    //  System.out.println("Topological Sort: "+Arrays.toString(L.toArray()));
    }
  }
  
  
}
